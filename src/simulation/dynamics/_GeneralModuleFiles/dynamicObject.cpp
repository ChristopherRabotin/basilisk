/*
 ISC License

 Copyright (c) 2016-2018, Autonomous Vehicle Systems Lab, University of Colorado at Boulder

 Permission to use, copy, modify, and/or distribute this software for any
 purpose with or without fee is hereby granted, provided that the above
 copyright notice and this permission notice appear in all copies.

 THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

 */

#include "dynamicObject.h"

/*! This is the constructor, just setting the variables to zero */
DynamicObject::DynamicObject()
{
    return;
}

/*! This is the destructor, nothing to report here */
DynamicObject::~DynamicObject()
{
    return;
}

/*! This method initializes the stateEffectors and dynamicEffectors and links the necessarry components together */
void DynamicObject::initializeDynamics()
{
    // - Specify iterators for both stateEffectors and dynamicEffectors
    std::vector<StateEffector*>::iterator it;
    std::vector<DynamicEffector*>::iterator dynIt;

    // - Loop over stateEffectors and register their states with the dynamic Manager
    for(it = states.begin(); it != states.end(); it++)
    {
        (*it)->registerStates(dynManager);
    }

    // - Loop over stateEffectors to allow the state effectors to have access to the requested states
    for(it = states.begin(); it != states.end(); it++)
    {
        (*it)->linkInStates(dynManager);
    }

    // - Loop over stateEffectors to allow the state effectors to have access to the requested states
    for(dynIt = dynEffectors.begin(); dynIt != dynEffectors.end(); dynIt++)
    {
        (*dynIt)->linkInStates(dynManager);
    }

    return;
}

/*! This method allows a dynamicObject to compute energy and momentum. Great for sim validation purposes */
void DynamicObject::computeEnergyMomentum(double t)
{
    return;
}

/*! This method attaches a stateEffector to the dynamicObject */
void DynamicObject::addStateEffector(StateEffector *newStateEffector)
{
    this->states.push_back(newStateEffector);

    return;
}

/*! This method attaches a dynamicEffector to the dynamicObject */
void DynamicObject::addDynamicEffector(DynamicEffector *newDynamicEffector)
{
    this->dynEffectors.push_back(newDynamicEffector);

    return;
}

/*! This method changes the integrator in use (Default integrator: RK4) */
void DynamicObject::setIntegrator(StateVecIntegrator *newIntegrator)
{
    if (newIntegrator != nullptr) {
        delete integrator;
        integrator = newIntegrator;
    }

    return;
}
